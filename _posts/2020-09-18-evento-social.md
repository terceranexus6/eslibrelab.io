---
layout: 2020/post
section: post
category: 2020
title: Evento social esta tarde en Mozilla Hubs
---

Hoy viernes 18, a las 19:00 CEST, tendrá lugar el evento social de esLibre 2020. Como no nos podemos ver en el mundo físico, nos encontraremos en un mundo virtual: el que hemos creado para todos nosotros en [Mozilla Hubs](https://hubs.mozilla.com). Para poder disfrutar de él basta con un navegador (mucho mejor si en escritorio que en móvil): Firefox reciente funciona seguro, Chrome (o derivados) y Webkit suelen ir bien también. Si tienes un dispositivo de realidad virtual con navegador (por ejemplo, Oculus Quest o HTC Vive) podrás entrar también, y experimentar mucho mejor la realidad virtual. Pero lo dicho: un navegador de escritorio será suficiente para pasar un buen rato. ¡Vente con nosotros un rato, e invita a venir a quien quiera participar con buena voluntad de nuestra fiesta!

En el proceso de entrada podrás dar permisos para tu micrófono, y así poder charlar con los demás asistentes. Una vez estés en las salas virtuales, podrás compartir también tu cámara, si te apetece.

En las propias salas tenéis información sobre cómo funciona todo, pero para empezar a funcionar, algunas pistas:

* Puedes usar las flechas (arriba, abajo, derecha, izquierda) para moverte.
* Para girarte puedes usar el ratón, pulsando y arrastrando
* Para seleccionar, pulsa con el ratón

Más detalles:

* [Hubs Features](https://hubs.mozilla.com/docs/hubs-features.html)
* [Hubs Controls](https://hubs.mozilla.com/docs/hubs-controls.html)
* [Mozilla Hubs Documentation](https://hubs.mozilla.com/docs/welcome.html)

Vamos a estar en una serie de salas interconectadas. Cada sala está calculada para una capacidad máxima de unas 20 personas (aunque puede llegar a haber alguna más). Por eso vamos a repartir las entradas entre cuatro salas. Así que para empezar, tira un dado de cuatro caras para elegir uno de los siguientes enlaces:

* [Lobby1](https://hubs.mozilla.com/SAQj3Pw/eslibre-lobby1)
* [Lobby2](https://hubs.mozilla.com/kSnsyiX/eslibre-lobby2)
* [Lobby3](https://hubs.mozilla.com/cUgumzL/eslibre-lobby3)
* [Lobby4](https://hubs.mozilla.com/haMP6Ka/eslibre-lobby4)

Desde cualquiera de esas salas puedes llegar a cada una de las otras tres, busca "cuadros" en una pared con el nombre de las otras salas, y pulsando con el ratón sobre ellos. Estas cuatro salas son iguales.

Desde cada una de estas salas puedes ir también a otras veinte, también iguales entre sí, que también verás como "cuadros" en una pared. Te ponemos a continuación sus enlaces directos, pero por favor, no los uses salvo que trates de entrar en las salas anteriores y estén ya llenas. Es mucho mejor que entres por una de esas cuatro salas. Pero por si acaso:

* [Room1](https://hubs.mozilla.com/DV3V6iY/eslibre-room1)
* [Room2](https://hubs.mozilla.com/DV3V6iY/eslibre-room2)
* [Room3](https://hubs.mozilla.com/DV3V6iY/eslibre-room3)
* [Room4](https://hubs.mozilla.com/DV3V6iY/eslibre-room4)
* [Room5](https://hubs.mozilla.com/DV3V6iY/eslibre-room5)
* [Room6](https://hubs.mozilla.com/DV3V6iY/eslibre-room6)
* [Room7](https://hubs.mozilla.com/DV3V6iY/eslibre-room7)
* [Room8](https://hubs.mozilla.com/DV3V6iY/eslibre-room8)
* [Room9](https://hubs.mozilla.com/DV3V6iY/eslibre-room9)
* [Room10](https://hubs.mozilla.com/DV3V6iY/eslibre-room10)
* [Room11](https://hubs.mozilla.com/DV3V6iY/eslibre-room11)
* [Room12](https://hubs.mozilla.com/DV3V6iY/eslibre-room12)
* [Room13](https://hubs.mozilla.com/DV3V6iY/eslibre-room13)
* [Room14](https://hubs.mozilla.com/DV3V6iY/eslibre-room14)
* [Room15](https://hubs.mozilla.com/DV3V6iY/eslibre-room15)
* [Room16](https://hubs.mozilla.com/DV3V6iY/eslibre-room16)
* [Room17](https://hubs.mozilla.com/DV3V6iY/eslibre-room17)
* [Room18](https://hubs.mozilla.com/DV3V6iY/eslibre-room18)
* [Room19](https://hubs.mozilla.com/DV3V6iY/eslibre-room19)
* [Room20](https://hubs.mozilla.com/DV3V6iY/eslibre-room20)

