---
layout: 2020/post
section: post
category: 2020
title: Abierto el plazo para la propuesta de candidaturas esLibre 2021
---

Despúes de la celebración de esLibre 2020 de forma virtual, y un pequeño descanso, ya está publicada a llamada para la propuesta de candidaturas para la organización de esLibre 2021. Toda la información está disponible [aquí](https://gitlab.com/eslibre/coord/-/tree/master/propuestas/2021). 

Como novedad este año, se puede solicitar la celebración del evento en modalidad física o virtual. Por favor, revisa el documento completamente si tienes intención de presentar una propuesta y proporciona suficiente información para que la comunidad pueda evaluarla.

Recuerda que las propuestas se envían como Merge Requests al [repositorio de coordinación de esLibre](https://gitlab.com/eslibre/coord/). Puedes usar [la plantilla](https://gitlab.com/eslibre/coord/tree/master/propuestas/2021/plantilla.md) que proporcionamos al efecto y debes dejar el fichero en [la carpeta de propuestas 2021](https://gitlab.com/eslibre/coord/tree/master/propuestas/2021).

La fecha límite para el envío de propuestas es el 14 de Diciembre de 2020.

Os animamos a mantener entre todos vivo esLibre. ¡¡Propón una sede!!